package org.bitbucket.thofis.sdre;

import org.bitbucket.thofis.sdre.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tom on 5/14/16.
 */
public interface PersonRepository extends JpaRepository<Person,Long> {
}
