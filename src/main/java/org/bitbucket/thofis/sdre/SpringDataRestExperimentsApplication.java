package org.bitbucket.thofis.sdre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataRestExperimentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataRestExperimentsApplication.class, args);
	}
}
