package org.bitbucket.thofis.sdre.model;

import javax.persistence.*;

/**
 * Created by tom on 5/16/16.
 */
@Entity
public class Phonenumber {

    @Id
    @GeneratedValue
    private Long id;

    private String number;

    private Phonenumber() {}

    public Phonenumber(String number) {
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public boolean equalNumbers(Phonenumber other) {
        return this.getNumber().equals(other.getNumber());
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phonenumber that = (Phonenumber) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
