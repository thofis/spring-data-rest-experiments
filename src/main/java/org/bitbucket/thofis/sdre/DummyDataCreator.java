package org.bitbucket.thofis.sdre;

import org.bitbucket.thofis.sdre.model.Country;
import org.bitbucket.thofis.sdre.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by tom on 5/16/16.
 */
@Component
public class DummyDataCreator implements CommandLineRunner {

    static Map<String,Country> initialCountries = new HashMap<>();
    static List<Person> initialPersons = new ArrayList<>();

    static {
        initialCountries.put("Germany", new Country("Germany"));
        initialCountries.put("China", new Country("China"));
        initialCountries.put("France", new Country("France"));
        initialCountries.put("Canada", new Country("Canada"));
        initialCountries.put("Austria", new Country("Austria"));

        createPerson("Herta", "Müller", Arrays.asList("12345", "23456"), initialCountries.get("Germany"));
        createPerson("Mo", "Yan", Collections.emptyList(), initialCountries.get("China"));
        createPerson("Alice", "Munro", Arrays.asList("34567"), initialCountries.get("Canada"));
        createPerson("Patrick", "Modiano", Arrays.asList("45678", "56789", "78901"), initialCountries.get("France"));
        createPerson("Elfriede", "Jelinek", Arrays.asList("09876"), initialCountries.get("Austria"));
    }

    private static void createPerson(String firstName, String lastName, List<String> phonenumbers, Country germany) {
        Person person = new Person(firstName, lastName);
        phonenumbers.forEach(person::addPhonenumber);
        initialPersons.add(person);
    }

    @Autowired
    PersonRepository personRepository;

    @Autowired
    CountryRepository countryRepository;

    @Override
    public void run(String... strings) throws Exception {
        initialCountries.forEach((name, country)-> countryRepository.save(country));
        initialPersons.forEach(personRepository::save);
    }
}
