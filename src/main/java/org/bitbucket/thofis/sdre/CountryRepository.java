package org.bitbucket.thofis.sdre;

import org.bitbucket.thofis.sdre.model.Country;
import org.bitbucket.thofis.sdre.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by tom on 5/14/16.
 */
public interface CountryRepository extends JpaRepository<Country,Long> {
}
