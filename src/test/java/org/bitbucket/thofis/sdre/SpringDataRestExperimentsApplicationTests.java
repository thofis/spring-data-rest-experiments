package org.bitbucket.thofis.sdre;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.thofis.sdre.model.Country;
import org.bitbucket.thofis.sdre.model.Person;
import org.bitbucket.thofis.sdre.model.Phonenumber;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.net.URI;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringDataRestExperimentsApplicationTests {

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    CountryRepository countryRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    static List<Person> initialPersons = DummyDataCreator.initialPersons;
    static Map<String, Country> initialCountries = DummyDataCreator.initialCountries;

    @Before
    public void before() {

        // setup test data
        deleteFromDb();

        initialCountries.forEach((key, country) -> {
            String sql = "insert into country (name) values ('%s');";
            jdbcTemplate.execute(String.format(sql, country.getName()));
        });

        for (int i = 0; i < initialPersons.size(); i++) {
            final int personId = i;
            Person person = initialPersons.get(personId);
            String sqlTemplate = "insert into person (id, first_name,last_name) values(%d, '%s','%s');";
            jdbcTemplate.execute(String.format(sqlTemplate, personId, person.getFirstName(), person.getLastName()));
            person.getPhonenumbers().forEach((phone) -> {
                String sql = "insert into phonenumber (number, person_id) values ('%s', %d);";
                jdbcTemplate.execute(String.format(sql, phone.getNumber(), personId));
            });
        }

        // make restTemplate HAL+JSON capable
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new Jackson2HalModule());

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
        converter.setObjectMapper(mapper);

        restTemplate.getRestTemplate().setMessageConverters(Collections.singletonList(converter));
    }

    private void deleteFromDb() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "phonenumber");
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "person");
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "country");
    }

    @Test
    public void getPersons() {
        ResponseEntity<PagedResources<Person>> responseEntity = this.restTemplate.exchange("/api/persons", HttpMethod.GET, null, new ParameterizedTypeReference<PagedResources<Person>>() {
        });
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        PagedResources<Person> personResource = responseEntity.getBody();
        Collection<Person> persons = personResource.getContent();

        assertHasSamePersons(persons, initialPersons);
    }

    @Test
    public void postPerson() {
        Person person = new Person("Albert", "Camus");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Person> personEntity = new HttpEntity<>(person);
        ResponseEntity<Resource<Person>> personResposeEntity = restTemplate.exchange("/api/persons", HttpMethod.POST, personEntity, new ParameterizedTypeReference<Resource<Person>>() {
        });

        int count = JdbcTestUtils.countRowsInTableWhere(jdbcTemplate, "person", "last_name = 'Camus'");
        assertThat(count).isEqualTo(1);

        assertThat(personResposeEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        URI location = personResposeEntity.getHeaders().getLocation();

        Pattern validPersonResourceLocation = Pattern.compile(".*/api/persons/[0-9]+");
        assertThat(location.getPath()).matches(validPersonResourceLocation);
    }

    @Test
    public void getPerson() {
        Person expectedPerson = initialPersons.get(0);
        ResponseEntity<Resource<Person>> responseEntity = getPersonEntity(0);

        Person person = responseEntity.getBody().getContent();
        assertThat(person).isEqualTo(expectedPerson);

        Set<String> actualNumbers = person.getPhonenumbers().stream().map(pn -> pn.getNumber()).collect(Collectors.toSet());
        Set<String> expectedNumbers = expectedPerson.getPhonenumbers().stream().map(pn -> pn.getNumber()).collect(Collectors.toSet());
        assertThat(actualNumbers).isEqualTo(expectedNumbers);

        Link countryLink = responseEntity.getBody().getLink("country");
        Country country = this.restTemplate.getForObject(countryLink.getHref(), Country.class);
        assertThat(country).isEqualTo(expectedPerson.getCountry());
    }

    private ResponseEntity<Resource<Person>> getPersonEntity(int id) {
        String url = String.format("/api/persons/%d", id);
        ResponseEntity<Resource<Person>> responseEntity = this.restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Resource<Person>>() {
        });
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        return responseEntity;
    }


    @Test
    public void putPerson() {
        String url = "/api/persons/1";
        ResponseEntity<Resource<Person>> responseEntity = getPersonEntity(1);
        Person personToUpdate = responseEntity.getBody().getContent();
        Person expectedPerson = new Person("Thomas", "Mann");
        assertThat(personToUpdate.getFirstName()).isNotEqualTo(expectedPerson);

        personToUpdate.updateName(expectedPerson.getFirstName(), expectedPerson.getLastName());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Person> personEntity = new HttpEntity<>(personToUpdate);
        ResponseEntity<Resource<Person>> personResposeEntity = this.restTemplate.exchange(url, HttpMethod.PUT, personEntity, new ParameterizedTypeReference<Resource<Person>>() {
        });

        Person returnedPerson = personResposeEntity.getBody().getContent();
        assertThat(returnedPerson).isEqualTo(expectedPerson);
        Person updatedPerson = this.restTemplate.getForObject(url, Person.class);
        assertThat(updatedPerson).isEqualTo(expectedPerson);
    }

    @Test
    public void changeCountry() {
        String url = "/api/persons/1";
        ResponseEntity<Resource<Person>> responseEntity = getPersonEntity(1);
        Resource<Person> personResource = responseEntity.getBody();

        Link countryLink = personResource.getLink("country");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/uri-list");
        String uriList = String.format("http://localhost:8080/api/countries/%d", 5);
        HttpEntity<String> countryEntity = new HttpEntity<>(uriList);
//        ResponseEntity<Resource<Person>> responseEntity1 = this.restTemplate.exchange(countryLink.getHref(), HttpMethod.PUT, countryEntity, new ParameterizedTypeReference<Resource<Person>>() {
//        });
        this.restTemplate.put(countryLink.getHref(), uriList);

        Object countryLinkResult = this.restTemplate.getForObject(countryLink.getHref(), Object.class);
        System.out.println("bla"+countryLinkResult);
    }

    @Test
    public void addPhonenumber() {
        String url = "/api/persons/0";
        Person expectedPerson = initialPersons.get(0);
        ResponseEntity<Resource<Person>> responseEntity = getPersonEntity(0);

        Person person = responseEntity.getBody().getContent();
        String nr = "11111";
        person.addPhonenumber(nr);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Person> personEntity = new HttpEntity<>(person);
        ResponseEntity<Person> personResposeEntity = this.restTemplate.exchange(url, HttpMethod.PUT, personEntity, Person.class);

        Person updatedPerson = this.restTemplate.getForObject(url, Person.class);
        assertThat(updatedPerson.getPhonenumbers()).contains(new Phonenumber(nr));

    }

    @Test
    public void deletePhonenumber() {
        String url = "/api/persons/0";
        Person expectedPerson = initialPersons.get(0);
        ResponseEntity<Resource<Person>> responseEntity = getPersonEntity(0);

        Person person = responseEntity.getBody().getContent();
        int phoneNumbersBefore = person.getPhonenumbers().size();
        person.getPhonenumbers().remove(0);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Person> personEntity = new HttpEntity<>(person);
        ResponseEntity<Person> personResposeEntity = this.restTemplate.exchange(url, HttpMethod.PUT, personEntity, Person.class);

        Person updatedPerson = this.restTemplate.getForObject(url, Person.class);
        assertThat(updatedPerson.getPhonenumbers().size()).isEqualTo(phoneNumbersBefore - 1);

    }

    @Test
    public void updatePhonenumber() {
        String url = "/api/persons/0";
        Person expectedPerson = initialPersons.get(0);
        ResponseEntity<Resource<Person>> responseEntity = getPersonEntity(0);

        Person person = responseEntity.getBody().getContent();
        int phoneNumbersBefore = person.getPhonenumbers().size();
        String nr = "11111";
        Phonenumber updatedNumber = new Phonenumber(nr);
        person.getPhonenumbers().get(0).setNumber(nr);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Person> personEntity = new HttpEntity<>(person);
        ResponseEntity<Person> personResposeEntity = this.restTemplate.exchange(url, HttpMethod.PUT, personEntity, Person.class);

        Person updatedPerson = this.restTemplate.getForObject(url, Person.class);
        assertThat(updatedPerson.getPhonenumbers().size()).isEqualTo(phoneNumbersBefore);
        assertThat(updatedPerson.getPhonenumbers().stream().anyMatch(updatedNumber::equalNumbers)).isTrue();

    }


    @Test
    public void deletePerson() {
        String url = "/api/persons/1";
        ResponseEntity<Person> personResponseEntity = this.restTemplate.getForEntity(url, Person.class);
        assertThat(personResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        int countBefore = JdbcTestUtils.countRowsInTable(jdbcTemplate, "person");

        this.restTemplate.delete(url);

        int countAfter = JdbcTestUtils.countRowsInTable(jdbcTemplate, "person");
        assertThat(countAfter).isEqualTo(countBefore - 1);

        personResponseEntity = this.restTemplate.getForEntity(url, Person.class);
        assertThat(personResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    }


    private boolean assertHasSamePersons(Collection<Person> actual, Collection<Person> expected) {
        return actual.stream().anyMatch(expected::contains) && expected.stream().anyMatch(actual::contains);
    }

}
